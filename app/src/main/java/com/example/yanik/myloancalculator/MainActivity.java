package com.example.yanik.myloancalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends Activity {
    private double loanAmount;
    private int term;
    private double interest;

    private EditText etLoanAmount;
    private EditText etTerm;
    private EditText etInterest;

    private TextView tvMonthlyPayment;
    private TextView tvTotalPayment;
    private TextView tvTotalInterest;
    private TextView tvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etLoanAmount = (EditText) findViewById(R.id.loan);
        etTerm = (EditText) findViewById(R.id.term);
        etInterest = (EditText) findViewById(R.id.interest);

        tvMonthlyPayment = (TextView) findViewById(R.id.monthly_payment);
        tvTotalPayment = (TextView) findViewById(R.id.total_payment);
        tvTotalInterest = (TextView) findViewById(R.id.total_interest);

        tvError = (TextView) findViewById(R.id.error);
    }

    public void calculate(View v) {
        if (getNumbers()) {
            LoanCalculator calculator = new LoanCalculator(loanAmount, term, interest);

            tvMonthlyPayment.setText(Double.toString(calculator.getMonthlyPayment()));
            tvTotalPayment.setText(Double.toString(calculator.getTotalCostOfLoan()));
            tvTotalInterest.setText(Double.toString(calculator.getTotalInterest()));
        }
    }

    public void clear(View v) {
        loanAmount = 0;
        term = 0;
        interest = 0;

        etLoanAmount.setText("");
        etTerm.setText("");
        etInterest.setText("");

        tvMonthlyPayment.setText("");
        tvTotalPayment.setText("");
        tvTotalInterest.setText("");

        tvError.setText("");
    }

    private boolean getNumbers() {
        if (etLoanAmount.getText().toString().isEmpty() ||
                etTerm.getText().toString().isEmpty() ||
                etInterest.getText().toString().isEmpty()) {
            tvError.setText(R.string.fields_empty);

            return false;
        } else {
            tvError.setText("");

            loanAmount = Double.parseDouble(etLoanAmount.getText().toString());
            term = Integer.parseInt(etTerm.getText().toString());
            interest = Double.parseDouble(etInterest.getText().toString());

            return true;
        }
    }
}
